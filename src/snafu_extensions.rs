use serde::Serializer;
use std::fmt::Debug;

pub fn debug_serialize<S>(x: &dyn Debug, s: S) -> std::result::Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(&format!("{:?}", x))
}
